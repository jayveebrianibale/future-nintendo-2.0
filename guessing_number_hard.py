import os
import random
print ("                                      ╔═╗╦ ╦╔═╗╔═╗╔═╗  ╔╦╗╦ ╦╔═╗  ╔╗╔╦ ╦╔╦╗╔╗ ╔═╗╦═╗")
print ("                                      ║ ╦║ ║║╣ ╚═╗╚═╗   ║ ╠═╣║╣   ║║║║ ║║║║╠╩╗║╣ ╠╦╝")
print ("                                      ╚═╝╚═╝╚═╝╚═╝╚═╝   ╩ ╩ ╩╚═╝  ╝╚╝╚═╝╩ ╩╚═╝╚═╝╩╚═")

target = random.randint(1, 1000)

retry = 0

while (True):
    print("Choose a number 1-1000 and START GUESSING!")
    choice = int(input("Enter your choice: "))
    retry += 1
    
    if target != choice:
        print ("Wrong Guess! Try Again!")
        
        if target < choice:
            print ("The required number lies between 0 and {}".format(choice))
        else: 
            print ("The required number lies between {} and 1000".format(choice))
        
    else:
        print ("You guess the correct number after {} retry".format(retry))
        break