import os
  
print("  _________________________________________________  ")
print("  |  ┌─┐┬ ┬┌─┐┌─┐┌─┐┌─┐  ┌┬┐┬┌─┐┌─┐┬┌─┐┬ ┬┬ ┌┬┐┬ ┬ | ")
print("  |  │  ├─┤│ ││ │└─┐├┤    │││├┤ ├┤ ││  │ ││  │ └┬┘ | ")
print("  |  └─┘┴ ┴└─┘└─┘└─┘└─┘  ─┴┘┴└  └  ┴└─┘└─┘┴─┘┴  ┴  | ")
print("  |                                                | ")
print("  |                  [1] Easy                      | ")
print("  |                  [2] Medium                    | ")
print("  |                  [3] Hard                      | ")
print("  |                  [4] Exit                      | ")
print("  |_______________________________________________ | ")

choice = int(input ("Enter your choice: "))
if choice == 1:
    choice = os.system(' python guessing_number_easy.py')
elif choice == 2:
    choice = os.system('python guessing_number_medium.py')
elif choice == 3:
   choice = os.system(' python guessing_number_hard.py')
elif choice == 4:
   choice = os.system(' python guessing_number_menu.py')
else:
    print("Invalid Choice: ")